using Godot;
using System;

public class TestLabel : Label
{
	[Export]
	public Resource Capitalized;

	public override void _Ready()
	{
		if (Capitalized is CapitalText csmodel)
			Text = csmodel.Text;
		else
			Text = Capitalized.Get("text") as string;
	}
}
