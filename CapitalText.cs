using Godot;
using System;

public class CapitalText : Resource
{
	[Export]
	public string Text {get; set;}
}
