using Godot;
using System;
#if TOOLS
[Tool]
public class plugin : EditorPlugin
{
	private Importer importer;

	public override void _EnterTree()
	{
		AddCustomType(nameof(CapitalText), nameof(Resource), (Script)GD.Load("res://CapitalText.cs"), null);
		AddImportPlugin(importer = new Importer());
	}

	public override void _ExitTree()
	{
		RemoveImportPlugin(importer);
		importer = null;
		RemoveCustomType(nameof(CapitalText));
	}
}
#endif
