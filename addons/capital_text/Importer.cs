using Godot;
using Godot.Collections;

public class Importer : EditorImportPlugin
{
	enum Presets
	{
		Mono,
		GDScript
	}

	public override string GetImporterName() => "wscalf.resourcesaverrepro";
	public override string GetVisibleName() => "Capital Text";
	public override Array GetRecognizedExtensions() => new Array() { "txt" };
	public override string GetSaveExtension() => "tres";
	public override string GetResourceType() => nameof(Resource);

	public override Array GetImportOptions(int preset)
	{
		return new Array()
		{
			new Dictionary()
			{
				["name"] = "use_gdscript_model",
				["default_value"] = (preset == (int)Presets.GDScript),
			}
		};
	}

	public override int GetPresetCount() => 2;

	public override string GetPresetName(int preset)
	{
		switch (preset)
		{
			case (int)Presets.Mono:
				return "C#";
			case (int)Presets.GDScript:
				return "GDScript";
			default:
				return "Missing case branch";
		}
	}

	public override bool GetOptionVisibility(string option, Dictionary options) => true;

	public override int Import(string sourceFile, string savePath, Dictionary options, Array platformVariants, Array genFiles)
	{
		Error result;

		var file = new File();
		result = file.Open(sourceFile, File.ModeFlags.Read);
		if (result != Error.Ok) return (int)result;

		var source = file.GetAsText();
		file.Close();

		Resource resource;
		if ((bool)options["use_gdscript_model"])
		{
			resource = (Resource)GD.Load("res://uppercase_text.gd").Call("new");
			resource.Set("text", source.ToUpper());
		}
		else
		{
			resource = new CapitalText() {Text = source.ToUpper()};
		}

		result = ResourceSaver.Save($"{savePath}.{GetSaveExtension()}", resource);
		if (result != Error.Ok) return (int)result;

		return (int)Error.Ok;
	}
	
}
